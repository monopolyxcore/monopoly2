

#include "XtremeMonopoly.h"
#include "XtremeMonopolyGameMode.h"
#include "XtremeMonopolyPlayerController.h"

AXtremeMonopolyGameMode::AXtremeMonopolyGameMode(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	PlayerControllerClass = AXtremeMonopolyPlayerController::StaticClass();
}


